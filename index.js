//Mathematical Operations (-, *, /, %)

//Subtraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3); 			//-0.5 results in proper mathematical operation
console.log(num3-num4); 			//5 results in proper mathematical operation
console.log(numString1-num2); 		// -1 results in proper mathematical operation because the string was forced to become a number (forced coercion)
console.log(numString2-num2);
console.log(numString1-numString2); // -1 in subtraction, numeric strings will not concatenate and instead will be forcibly changed its type and subtract properly

let sample2 = "Juan Dela Cruz";

console.log(sample2-numString1); //results in NaN - not a number. When trying to perform subtraction between alphanumeric string and numeric string, results in NaN

//Multiplication
console.log(num1*num2);					//30
console.log(numString1*num1);			//25
console.log(numString1*numString2);		//30
console.log(sample2*5);					//NaN

let product = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product);

//Division
console.log(product / num2);			//5
console.log(product2 / 5);				//5
console.log(numString2 / numString1);	//1.2
console.log(numString2 % numString1);	//1 modulo: gets the remainder

//Division/Multiplication by 0
console.log(product2 * 0);				//0
console.log(product3 * 0);				//0
//Division by 0 is not accurately and should not be done because it results to infinity
console.log(product2 / 0);				//Infinity

//Modulo (%) remainder of division operation
console.log(product2 % num2); 			//(25/6) remainder 1
console.log(product3 % product2);		//5
console.log(num1 % num2);				//5
console.log(num1 % num1);				//0

//Boolean (true or false)
/*
	Boolean is usually used for logic operations or if-else conditions
	Boolean values are normally used to store values relating to the state
	When creating a variable which will contain a boolean, the variable name is usually a yes or no question
	Example: isRegistered
*/
let isAdmin = true;
let isMarried = false;
let isMVP = true;

//You can also concatenate strings + boolean
console.log("Is she married? " + isMarried);
console.log("Is he the MVP? " + isMVP);
console.log(`Is he the current Admin? ${isAdmin}`);

//Arrays
/*
	Arrays are a special kind of data type to store multiple values
		- can actually store data with different types but as the best practice, arrays are used to contain multiple values of the SAME data type
		- values in array are separated by commas
	An Array is created with an array literal = []
*/

//Syntax:
	//let/const arrayName = [elementA, elementB, elementC, ...];

let array1 = ["Goku", "Picolo", "Gohan", "Vegeta"];			//Same data type (best practice)
console.log(array1);

let array2 = ["One Punch Man", true, 500, "Saitama"];		//Different data type
console.log(array2);

//Arrays are better thought of as group of data

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//Objects
/*
	Objects
		- are another special kind of data type used to mimic the real world
		- used to create complex data that contain pieces of information that are relevant to each other
		- objects are created with object literals = {}
			- each data/value are paired with a key
			- each field is called a property
			- each field is separated by a comma

	Syntax:
	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+639154847451", "8123 4567"],
	address: {
		houseNumber: '345',
		street: 'Diamond',
		city: 'Manila'
	}
};

console.log(person);


/*Mini-Activity
	Create a variable with a group of data
		- The group of data should contain names from your favorite band

	Create a variable which contain multiple values of differing types and describe a single person
		- This data type should be able to contain multiple key value pairs:
			firstName: <value>
			lastName: <value>
			isDeveloper: <value>
			hasPortfolio: <value>
			age: <value>
			contact: <value>, <value>
			address: {
				houseNumber: <value>
				street: <value>
				city: <value>
			}
*/

//Mini-Activity Serrano, Sean Patrick D. BCS21
let LoveHandel = ["Sherman", "Danny", "Bobbi Fabulous"]

let bassPlayer = {
	firstName: "Bobbi",
	lastName: "Fabulous",
	isDeveloper: false,
	hasPortfolio: true,
	age: 30,
	contact: ["(415)555-2671", "+14155552671"],
	address: {
		houseNumber: "2308",
		street: "Maple Drive",
		city: "Danville"
	}
};

console.log(LoveHandel);
console.log(bassPlayer);


//Undefined vs Null
/*
	Null 
	- is explicit absence of data/value. This is done to project that a variable contains nothing over undefined
	
	Undefined 
	- merely means there is no data in the variable because the variable has not been assigned an initial value
	- is a representation that a variable has been declared but it was not assigned an initial value
*/

let sampleNull = null;
console.log(sampleNull);

let sampleUndefined;
console.log(sampleUndefined);

//Certain processes in programming explicitly returns null to indicate that the task resulted to nothing

let foundResult = null;

//Example:
let myNumber = 0;
let myString = " ";
//using null to compared to a 0 value and and empty string is much better for readability;

//For undefined, this is normally caused by developers creating variables that have no value or data associated with them

let person2 = {
	name: "Peter",
	age: 35
}

console.log(person2.isAdmin);
//results in undefined because person2 does exist but the property isAdmin does not exist



/* [SECTION] Functions
	Functions
	- in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	- are reusable pieces of code with instructions which is used over and over again as long as we can call or invoke them

	Syntax:
		function functionName(){
			code block
			- the block of code that will be executed once the function has been run/called/invoked
		}
*/

//Declaring Function
function printName(){
	console.log("My name is Jin");
};

//Invoking/calling of function
printName();

function showSum(){
	console.log(25+6);
};

showSum();
//Note: do not create functions with the same name 


/*Parameters and Arguments
	"name" is called a parameter
		a parameter acts as a named variable that exists only inside of the function
		this is used to store information or act as a stand-in or the container for the value passed into the function as an argument
*/

function printName(name){
	console.log(`My name is ${name}`);
};

//When a function is onvoked and data is apssed, we call the data as argument
//In this invocation, "V" is an argument passed into our printName function and is represented by the "name" parameter within our function
printName("V");

//Data passed into the function: argument
//Representation of the argument within the function: parameter


function displayNum(number){
	alert(number);
};

displayNum(1000);

/*Mini-Activity
	Create a function which will be able to show message in the console.
	The message should be able to be passed into the function via an argument

		- Sample message:
			"JavaScript is Fun"
			"C++ is Fun"
			
		Argument: programming language
*/

//Mini-Activity Serrano, Sean Patrick D. BCS21
function showMessage(lang){
	console.log(`${lang} is Fun!`);
};

showMessage("JavaScript");
showMessage("C++");
showMessage("Python");



//Multiple parameters and arguments
function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
};

displayFullName("Seok-jin", "P.", "Kim", "29");


//Return statement
	//The 'return' statement allows the output of a function to be passed to the line/block of code that invoked/called the function
	//Any line or block of code that comes after the return statement is ignored because it ends the function execution

function createFullName(firstName, middleName, lastName){
	//return keyword is used so that a function may return a value
	return `${firstName} ${middleName} ${lastName}`;
	console.log("I will no longer run because the function's value/result has been returned.")
	//console.log will not run because it already returned the value
};

let fullName1 = createFullName("Jungkook", "BTS", "Jeon");
console.log(fullName1);

let fullName2 = displayFullName("Tom", "Mapother", "Cruise");
console.log(fullName2);
//results in undefined because displayFullName doesn't have a return statement

let fullName3 = createFullName("Tae-hyung", "BTS", "Kim");
console.log(fullName3);









































