//Activity Serrano, Sean Patrick D. BCS21
function add (num1, num2){
	console.log(`${num1} + ${num2} = ${num1+num2}`);
};

function subtract (num1, num2){
	console.log(`${num1} - ${num2} = ${num1-num2}`);
};

function multiply (num1, num2){
	return num1 * num2;
};

add(2, 3);
subtract(10, 5);

let product = multiply(4, 6);
console.log(product);